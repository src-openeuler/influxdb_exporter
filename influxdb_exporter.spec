%undefine _missing_build_ids_terminate_build

Name:           influxdb_exporter
Version:        0.11.5
Release:        1
Summary:        A server that accepts InfluxDB metrics for prometheus consumption
License:        Apache-2.0 and MIT
URL:            https://github.com/prometheus/influxdb_exporter
Source0:        https://github.com/prometheus/influxdb_exporter/archive/v%{version}/%{name}-%{version}.tar.gz
# tar -xvf Source0
# run 'go mod vendor' in it
# tar -czvf influxdb_exporter-vendor.tar.gz vendor
Source1:        influxdb_exporter-vendor.tar.gz
Patch0:         0001-using-vendor.patch

BuildRequires:  make promu
BuildRequires:  golang >= 1.13

%description
A server that accepts InfluxDB metrics via the HTTP API and exports them via HTTP for Prometheus consumption

%prep
%autosetup -b0 -a1 -n %{name}-%{version} -p1

%build
promu build

%install
install -d -p %{buildroot}%{_bindir}
install -m 755 %{_builddir}/%{name}-%{version}/%{name}-%{version} %{buildroot}%{_bindir}/influxdb_exporter

%files
%license LICENSE
%doc README.md
%{_bindir}/influxdb_exporter

%changelog
* Thu Dec 28 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.11.5-1
- Update package to version 0.11.5

* Tue Jan 31 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.11.2-1
- Update package to version 0.11.2

* Mon Jun 13 2022 lijian <lijian2@kylinos.cn> - 0.9.1-1
- Package init
